import java.nio.file.Paths

plugins {
    id( "com.github.johnrengelman.shadow" ) version "7.1.2"
    kotlin("jvm") version "1.7.20"
    idea
}

group = "com.enderzombi102"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
    maven( url = "https://jitpack.io" )
    maven( url = "https://maven.minecraftforge.net" )
}

val shade: Configuration by configurations.creating { }
dependencies {
    implementation( "com.github.Col-E:Recaf:2.21.13" )
}

tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}

tasks.withType<com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar> {
    configurations = listOf( shade )
    archiveClassifier.set("")

    from( "LICENSE" ) {
        rename { "${it}_$archiveBaseName}" }
    }
}

tasks.register<JavaExec>( "run" ) {
    mainClass.set("me.coley.recaf.Recaf")
    classpath = sourceSets.main.get().runtimeClasspath

    dependsOn( tasks.jar )

    doFirst {
        tasks.jar.get().outputs.files.files.forEach { file ->
            file.copyTo( Paths.get( System.getenv("APPDATA"), "Recaf/plugins/${file.name}" ).toFile(), true )
        }
    }

    doLast {
        tasks.jar.get().outputs.files.files.forEach { file ->
            Paths.get( System.getenv("APPDATA"), "Recaf/plugins/${file.name}" ).toFile().delete()
        }
    }
}
