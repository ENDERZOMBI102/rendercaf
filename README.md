Rendercaf
-
A plugin for [Recaf](https://www.coley.software/Recaf) that improves stuff.

Features:
 - [ ] Auto-deobfuscate minecraft mods
   - [ ] FabricMC
   - [ ] QuiltMC
   - [ ] Minecraft Forge
 - [ ] Adds the QuiltFlower decompiler
 - [ ] Some more QoL stuff