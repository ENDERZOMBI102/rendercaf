package com.enderzombi102.rendercaf

import me.coley.recaf.plugin.api.BasePlugin
import org.plugface.core.annotations.Plugin

@Plugin(name="RenderCaf")
class RenderCafPlugin : BasePlugin {
    override fun getVersion(): String = "1.0"

    override fun getDescription(): String = "A template plugin to work off of";
}